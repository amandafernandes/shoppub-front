# Frontend Test

O objetivo do desafio é simples: Montar e estruturar um carrossel de produtos utilizando HTML e CSS, seguindo o layout fornecido.
Você poderá exibir e listar os produtos consumindo o arquivo "produtos.json", via JavaScript puro ou alguma biblioteca como JQuery, por exemplo.

## Design

- Utilizamos o Adobe XD para nossos layouts.  
  https://www.adobe.com/br/products/xd.html
- O carrossel apresentado deve seguir o layout contido no arquivo shoppub-front.xd (Adobe XD).
- No arquivo shoppub-front.xd você encontrará como deve ficar no mobile e desktop.


## Como realizar o teste

- Crie um Readme com uma descrição de como rodar seu projeto
- Descreva as funcionalidades do seu desafio, venda seu peixe! Conte-nos o que utilizou, qual foi a metodologia escolhida. 
  Por exemplo, nos diga se fez o carrossel consumindo o arquivo .json de produtos via JavaScript e/ou, se utilizou o Grunt, quais foram os módulos presentes no seu Gruntfile.js
- Em caso de dúvidas, entre em contato com amanda.fernandes@shoppub.com.br

## Dicas
 
- Os ícones estão em SVG.
- A fonte utilizada é https://fonts.google.com/specimen/Poppins
- Amamos CSS responsivo, organizado, modular e feito com pré-processadores. Utilizamos o Grunt aqui na empresa e seguimos a filosofia: desenvolver CSS com SCSS e SMACSS faz BEM, rs
- Tenha atenção com espaçamentos, tamanhos e estilos de fonte. 
- Trabalhamos com mobile first.

## Critérios de avaliação

- CSS. Sem Frameworks!
- Qualidade no código.
- Fidelidade ao design proposto.
- Adaptação mobile.
